// SPDX-License-Identifier: MIT
/*
  BasicProtothreading

  A simple example of using the library. Demonstrates pseudo parallel execution
  of two tasks.

  Created 24.01.2023
  By Jakub Švarc <jakubsvarc@protonmail.com>

  https://codeberg.org/atlas144/protorduino
*/

#include <Protorduino.h>

/*
Initialize Protorduino with 2 expected tasks. Number of expected tasks is
the minimum number that will be registered. It is possible to register more.
*/
Protorduino protorduino(2);

// First example task.
void taskOne() {
  Serial.println("This is task one!");
}

// Second example task.
void taskTwo() {
  Serial.println("This is task two!");
}

void setup() {
  // Serial is used here only to demonstrate the tasks action.
  Serial.begin(9600);

  /*
  Registration of tasks. The numbers (1000 and 5000) means number of
  miliseconds between executions of each task. That means - the first task
  is executed every 1000 miliseconds.
  */
  protorduino.registerTask(taskOne, 1000);
  protorduino.registerTask(taskTwo, 5000);
}

void loop() {
  /*
  It must always be in the loop block. It performs the inner work needed
  for protothreading.
  
  Generally, it is a good idea not to have any additional code in the loop.
  Ideally, everything should be in tasks.
  */
  protorduino.loop();
}
