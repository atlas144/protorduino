# Protorduino

![Version](https://img.shields.io/badge/dynamic/json?color=informational&label=version&query=%24.general.version.sw&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2Fprotorduino%2Fraw%2Fbranch%2Fmain%2Fshields.json)
[![License](https://img.shields.io/badge/dynamic/json?color=green&label=license&query=%24.general.license.sw&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2Fprotorduino%2Fraw%2Fbranch%2Fmain%2Fshields.json)](https://codeberg.org/atlas144/protorduino/src/branch/main/LICENSE)
![Language](https://img.shields.io/badge/dynamic/json?color=red&label=language&query=%24.general.language&url=https%3A%2F%2Fcodeberg.org%2Fatlas144%2Fprotorduino%2Fraw%2Fbranch%2Fmain%2Fshields.json)

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

## Description

Simple Arduino library implementation of protothreads, to enable pseudo parallel execution of tasks.

### Working principle

The individual blocks of code that are to be executed in parallel are placed in separate functions, called *tasks*. These are executed repeatedly, always after a specified time interval has elapsed.

## Using

First, we need to create *task* functions that will not contain any parameters and will not return any value. Tasks must be executed as fast as possible, so they must not contain any blocking code (e.g. `delay()`):

```cpp
void taskOne() {
    // do something
}

void taskTwo() {
    // do something else
}
```

Then we create an instance of the library main class and register our *tasks*, ideally in the `setup()`:

```cpp
// Library main class instance. The number means minimal number of registered tasks.
Protorduino protorduino(2);

void setup() {
    // Task registration. taskOne is reference to our task and 1000 is number of milliseconds between each execution of the task.
    protorduino.registerTask(taskOne, 1000);
    protorduino.registerTask(taskTwo, 5000);
}
```

Finally, we put the library `loop()` function into the program `loop()`, which takes care of invoking the task at the right time:

```cpp
void loop() {
    protorduino.loop();
}
```

Ideally, the program `loop()` function should not contain any other code, everything should be placed in the *tasks*.

## Dependencies

None.

## Contributing

Contributions are welcome!

## License

The source code is licensed under the [MIT](https://codeberg.org/atlas144/protorduino/src/branch/main/LICENSE) license.
