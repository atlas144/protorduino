// SPDX-License-Identifier: MIT

#ifndef TASK_H
#define TASK_H

#include <Arduino.h>

typedef void (*TaskAction)();

struct Task {
    TaskAction taskAction;
    unsigned long taskDuration;
    unsigned long lastExecutionTimestamp;
};

#endif
