###########################################
# Syntax Coloring Map For Protorduino library
###########################################

###########################################
# Datatypes (KEYWORD1)
###########################################

Protorduino	    KEYWORD1
Task            KEYWORD1

###########################################
# Methods and Functions (KEYWORD2)
###########################################

registerTask    KEYWORD2
loop            KEYWORD2