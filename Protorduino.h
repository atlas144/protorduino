// SPDX-License-Identifier: MIT

#ifndef PROTORDUINO_H
#define PROTORDUINO_H

#include <Arduino.h>
#include "Task.h"

typedef void (*TaskAction)();

class Protorduino {
    private:
        byte expectedNumberOfTasks;
        byte numberOfTasks;
        Task* tasks;

    public:
        Protorduino(byte expectedNumberOfTasks);
        void registerTask(TaskAction taskAction, unsigned long tasksDuration);
        void loop();
};

#endif
