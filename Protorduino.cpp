// SPDX-License-Identifier: MIT

#include "Protorduino.h"

Protorduino::Protorduino(byte expectedNumberOfTasks) : expectedNumberOfTasks(expectedNumberOfTasks), numberOfTasks(0), tasks(new Task[expectedNumberOfTasks]) {}

void Protorduino::registerTask(TaskAction taskAction, unsigned long tasksDuration) {
    if (numberOfTasks >= expectedNumberOfTasks) {
        Task* newTasks = new Task[++expectedNumberOfTasks];

        for (byte i = 0; i < numberOfTasks; ++i) {
            newTasks[i] = tasks[i];
        }

        delete [] tasks;
        tasks = newTasks;
    }

    tasks[numberOfTasks].taskAction = taskAction;
    tasks[numberOfTasks].taskDuration = tasksDuration;
    tasks[numberOfTasks].lastExecutionTimestamp = 0;
    ++numberOfTasks;
}

void Protorduino::loop() {
    for (byte i = 0; i < numberOfTasks; ++i) {
        unsigned long currentTimestamp = millis();

        if (tasks[i].lastExecutionTimestamp + tasks[i].taskDuration <= currentTimestamp) {
            tasks[i].taskAction();
            
            tasks[i].lastExecutionTimestamp = currentTimestamp;
        }
    }
}
